module.exports = function (grunt) {
    'use strict';

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    var appConfig = {
        app: 'app', dist: 'dist'
    };


    var serveStatic = require('serve-static');

    // Grunt configuration
    grunt.initConfig({
        appConfig: appConfig,
        // The grunt server settings
        connect: {
            options: {
                port: 9000,
                hostname: '127.0.0.1',
                livereload: 35729
            },
            livereload: {
                options: {
                    open: true,
                    middleware: function (connect) {
                        return [
                            require('json-proxy').initialize({
                                proxy: {
                                    forward: {
                                        '/svcRest/': 'http://sandbox.dev.2brains.cl'
                                    },
                                    headers: {
                                        'BCIAPPID': 'personas',
                                        'BCIAPPVERSION': '1'
                                    }
                                }
                            }),
                            serveStatic('.tmp'),
                            connect().use(
                                '/bower_components',
                                serveStatic('./bower_components')
                            ),
                            serveStatic(appConfig.app)
                        ];
                    }
                }
            }
        },
        watch: {
            livereload: {
                options: {
                    livereload: '<%= connect.options.livereload %>'
                },
                files: [
                    '<%= appConfig.app %>/**/*.html',
                    '<%= appConfig.app %>/**/*.js',
                ]
            }
        },
        copy: {
            dist: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= appConfig.app %>',
                        dest: '<%= appConfig.dist %>',
                        src: [
                            '*.html',
                        ]
                    },
                ]
            }
        },
        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '.tmp',
                        'dist/**/*.*'
                    ]
                }]
            }
        },
        filerev: {
            dist: {
                src: [
                    'dist/scripts/**/*.js',
                ]
            }
        },
        uglify: {
            options : {
                beautify : false,
                mangle   : true
            }
        },
        useminPrepare: {
            html: 'app/index.html',
            options: {
                dest: 'dist'
            }
        },
        usemin: {
            html: ['dist/index.html']
        }

    });

    grunt.registerTask('live', [
        'connect:livereload',
        'watch'
    ]);

    grunt.registerTask('server', [
        'build',
        'connect:dist:keepalive'
    ]);

    grunt.registerTask('build', [
        'clean:dist',
        'copy:dist',
        'useminPrepare',
        'concat',
        'uglify',
        'filerev',
        'usemin',
    ]);

};