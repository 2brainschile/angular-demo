(function() {

  function ModalInstanceCtrl($scope, $http, AuthService, $uibModal) {
  }

  angular
  .module('sampleApp')
  .controller('ModalInstanceCtrl', ModalInstanceCtrl);
  
  function SampleCtrl($scope, $http, AuthService, $uibModal) {

    $scope.valorDolar = 560;

    $scope.movimientos = [
      {'glosa': 'Compra Amazon',
       'valor': 100},
      {'glosa': 'Pago Suscripción',
       'valor': 200},
      {'glosa': 'Compra eBay',
       'valor': 120}
    ];

    $scope.llamar = function() {
    
      var modalInstance = $uibModal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'modal.html',
        controller: 'ModalInstanceCtrl',
        size: "small"
      });

      /*
      AuthService.login("11111111", "1").then(
          function successCallback(response) {
              console.error(response)
          }, 
          function errorCallback(response) {
              console.err("test")
          }
      );
      */

    };

  }

  angular
  .module('sampleApp')
  .controller('SampleCtrl', SampleCtrl);


})();