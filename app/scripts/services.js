(function() {

  'use strict';

  var AuthService = function ($http) {
   
    this.login = function(rut, password) {
      
      return $http({
          method: 'POST',
          url: "/svcRest/sesionws",
          headers: {
              'Content-Type': "application/json"
          },
          data: {
            "rutCliente": rut,
            "pwCliente": password,
            "dvCliente": "1",
            "canal": "102",
            "srvInicial": "",
            "idAplicacion": "cl.bci.bancamovil.personas"
        }
      });

    };

    return this;
  };

  angular
      .module('sampleApp')
      .factory('AuthService', AuthService);

})();